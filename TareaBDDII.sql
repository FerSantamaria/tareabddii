--Empezar con usuario system

alter session set "_ORACLE_SCRIPT"=true;

create user DEMs identified by 123456;
grant dba to DEMs;

--Salir de usuario system y conectarse a usuario DEMO

create table USUARIOS(
user_id number(5) generated always as identity primary key,
username varchar2(20),
pass varchar2(20),
perm varchar2(10)
)

create table REGISTROS(
id_registro number(5) generated always as identity primary key,
nombres varchar2(20),
apellidos varchar2(20)
)

insert into USUARIOS(username,pass,perm) values ('admin','123456','admin');
insert into USUARIOS(username,pass,perm) values ('user','abcd','user');

insert into REGISTROS(nombres,apellidos) values ('Alejandra', 'Gomez');

-- prod para ingresar
  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "DEMS"."INSERTAR" (nom in VARCHAR2, apell in varchar2)
as 
Begin
insert into REGISTROS (nombres, apellidos) values (nom,apell);
end;


--prod para actualizar 
  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "DEMS"."ACTUALIZAR" 
(
  acod IN NUMBER 
, anom IN VARCHAR2 
, apell IN VARCHAR2 
) AS 
BEGIN
  update REGISTROS set nombres = anom, apellidos = apell where id_registro = acod;
  exception 
  when NO_DATA_FOUND then 
  null;
  when others then
  raise;
END ACTUALIZAR;

--prod para eliminar
  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "DEMS"."ELIMINAR" 
(
  ECOD IN NUMBER 
) AS 
BEGIN
  delete from REGISTROS where ID_REGISTRO = ecod;
END ELIMINAR;

COMMIT;