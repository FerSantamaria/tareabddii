﻿namespace TareaBDDII
{
    partial class frmAsistente
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAsistente));
            this.tabWizard = new System.Windows.Forms.TabControl();
            this.tabTipo = new System.Windows.Forms.TabPage();
            this.btnSiguienteTipo = new System.Windows.Forms.Button();
            this.rdbTablas = new System.Windows.Forms.RadioButton();
            this.rdbEsquema = new System.Windows.Forms.RadioButton();
            this.rdbCompleta = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.tabTablas = new System.Windows.Forms.TabPage();
            this.btnAnteriorTablas = new System.Windows.Forms.Button();
            this.cklTablas = new System.Windows.Forms.CheckedListBox();
            this.btnSiguienteTablas = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tabUbicacion = new System.Windows.Forms.TabPage();
            this.btnAnteriorUbicacion = new System.Windows.Forms.Button();
            this.btnSiguienteUbicacion = new System.Windows.Forms.Button();
            this.lblUbicacion = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnUbicacion = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tabConfirmacion = new System.Windows.Forms.TabPage();
            this.btnAnteriorConfirmacion = new System.Windows.Forms.Button();
            this.btnCopiar = new System.Windows.Forms.Button();
            this.txtResumen = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabGenerando = new System.Windows.Forms.TabPage();
            this.pgbCopia = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.tabWizard.SuspendLayout();
            this.tabTipo.SuspendLayout();
            this.tabTablas.SuspendLayout();
            this.tabUbicacion.SuspendLayout();
            this.tabConfirmacion.SuspendLayout();
            this.tabGenerando.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabWizard
            // 
            this.tabWizard.Controls.Add(this.tabTipo);
            this.tabWizard.Controls.Add(this.tabTablas);
            this.tabWizard.Controls.Add(this.tabUbicacion);
            this.tabWizard.Controls.Add(this.tabConfirmacion);
            this.tabWizard.Controls.Add(this.tabGenerando);
            this.tabWizard.Location = new System.Drawing.Point(12, 54);
            this.tabWizard.Name = "tabWizard";
            this.tabWizard.SelectedIndex = 0;
            this.tabWizard.Size = new System.Drawing.Size(440, 235);
            this.tabWizard.TabIndex = 0;
            // 
            // tabTipo
            // 
            this.tabTipo.Controls.Add(this.btnSiguienteTipo);
            this.tabTipo.Controls.Add(this.rdbTablas);
            this.tabTipo.Controls.Add(this.rdbEsquema);
            this.tabTipo.Controls.Add(this.rdbCompleta);
            this.tabTipo.Controls.Add(this.label1);
            this.tabTipo.Location = new System.Drawing.Point(4, 22);
            this.tabTipo.Name = "tabTipo";
            this.tabTipo.Padding = new System.Windows.Forms.Padding(3);
            this.tabTipo.Size = new System.Drawing.Size(432, 209);
            this.tabTipo.TabIndex = 0;
            this.tabTipo.Text = "Tipo";
            this.tabTipo.UseVisualStyleBackColor = true;
            // 
            // btnSiguienteTipo
            // 
            this.btnSiguienteTipo.Location = new System.Drawing.Point(351, 180);
            this.btnSiguienteTipo.Name = "btnSiguienteTipo";
            this.btnSiguienteTipo.Size = new System.Drawing.Size(75, 23);
            this.btnSiguienteTipo.TabIndex = 5;
            this.btnSiguienteTipo.Text = "Siguiente";
            this.btnSiguienteTipo.UseVisualStyleBackColor = true;
            this.btnSiguienteTipo.Click += new System.EventHandler(this.btnSiguienteTipo_Click);
            // 
            // rdbTablas
            // 
            this.rdbTablas.AutoSize = true;
            this.rdbTablas.Location = new System.Drawing.Point(9, 82);
            this.rdbTablas.Name = "rdbTablas";
            this.rdbTablas.Size = new System.Drawing.Size(102, 17);
            this.rdbTablas.TabIndex = 3;
            this.rdbTablas.TabStop = true;
            this.rdbTablas.Text = "Copia de Tablas";
            this.rdbTablas.UseVisualStyleBackColor = true;
            // 
            // rdbEsquema
            // 
            this.rdbEsquema.AutoSize = true;
            this.rdbEsquema.Location = new System.Drawing.Point(9, 59);
            this.rdbEsquema.Name = "rdbEsquema";
            this.rdbEsquema.Size = new System.Drawing.Size(114, 17);
            this.rdbEsquema.TabIndex = 2;
            this.rdbEsquema.TabStop = true;
            this.rdbEsquema.Text = "Copia de Esquema";
            this.rdbEsquema.UseVisualStyleBackColor = true;
            // 
            // rdbCompleta
            // 
            this.rdbCompleta.AutoSize = true;
            this.rdbCompleta.Checked = true;
            this.rdbCompleta.Location = new System.Drawing.Point(9, 36);
            this.rdbCompleta.Name = "rdbCompleta";
            this.rdbCompleta.Size = new System.Drawing.Size(99, 17);
            this.rdbCompleta.TabIndex = 1;
            this.rdbCompleta.TabStop = true;
            this.rdbCompleta.Text = "Copia Completa";
            this.rdbCompleta.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Seleccione el tipo de copia de seguridad:";
            // 
            // tabTablas
            // 
            this.tabTablas.Controls.Add(this.btnAnteriorTablas);
            this.tabTablas.Controls.Add(this.cklTablas);
            this.tabTablas.Controls.Add(this.btnSiguienteTablas);
            this.tabTablas.Controls.Add(this.label2);
            this.tabTablas.Location = new System.Drawing.Point(4, 22);
            this.tabTablas.Name = "tabTablas";
            this.tabTablas.Padding = new System.Windows.Forms.Padding(3);
            this.tabTablas.Size = new System.Drawing.Size(432, 209);
            this.tabTablas.TabIndex = 1;
            this.tabTablas.Text = "Tablas";
            this.tabTablas.UseVisualStyleBackColor = true;
            // 
            // btnAnteriorTablas
            // 
            this.btnAnteriorTablas.Location = new System.Drawing.Point(270, 180);
            this.btnAnteriorTablas.Name = "btnAnteriorTablas";
            this.btnAnteriorTablas.Size = new System.Drawing.Size(75, 23);
            this.btnAnteriorTablas.TabIndex = 9;
            this.btnAnteriorTablas.Text = "Anterior";
            this.btnAnteriorTablas.UseVisualStyleBackColor = true;
            this.btnAnteriorTablas.Click += new System.EventHandler(this.btnAnteriorTablas_Click);
            // 
            // cklTablas
            // 
            this.cklTablas.FormattingEnabled = true;
            this.cklTablas.Location = new System.Drawing.Point(6, 33);
            this.cklTablas.Name = "cklTablas";
            this.cklTablas.Size = new System.Drawing.Size(420, 94);
            this.cklTablas.TabIndex = 2;
            // 
            // btnSiguienteTablas
            // 
            this.btnSiguienteTablas.Location = new System.Drawing.Point(351, 180);
            this.btnSiguienteTablas.Name = "btnSiguienteTablas";
            this.btnSiguienteTablas.Size = new System.Drawing.Size(75, 23);
            this.btnSiguienteTablas.TabIndex = 8;
            this.btnSiguienteTablas.Text = "Siguiente";
            this.btnSiguienteTablas.UseVisualStyleBackColor = true;
            this.btnSiguienteTablas.Click += new System.EventHandler(this.btnSiguienteTablas_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Seleccione la tablas a respaldar:";
            // 
            // tabUbicacion
            // 
            this.tabUbicacion.Controls.Add(this.btnAnteriorUbicacion);
            this.tabUbicacion.Controls.Add(this.btnSiguienteUbicacion);
            this.tabUbicacion.Controls.Add(this.lblUbicacion);
            this.tabUbicacion.Controls.Add(this.txtNombre);
            this.tabUbicacion.Controls.Add(this.label6);
            this.tabUbicacion.Controls.Add(this.btnUbicacion);
            this.tabUbicacion.Controls.Add(this.label5);
            this.tabUbicacion.Location = new System.Drawing.Point(4, 22);
            this.tabUbicacion.Name = "tabUbicacion";
            this.tabUbicacion.Padding = new System.Windows.Forms.Padding(3);
            this.tabUbicacion.Size = new System.Drawing.Size(432, 209);
            this.tabUbicacion.TabIndex = 6;
            this.tabUbicacion.Text = "Ubicación";
            this.tabUbicacion.UseVisualStyleBackColor = true;
            // 
            // btnAnteriorUbicacion
            // 
            this.btnAnteriorUbicacion.Location = new System.Drawing.Point(270, 180);
            this.btnAnteriorUbicacion.Name = "btnAnteriorUbicacion";
            this.btnAnteriorUbicacion.Size = new System.Drawing.Size(75, 23);
            this.btnAnteriorUbicacion.TabIndex = 11;
            this.btnAnteriorUbicacion.Text = "Anterior";
            this.btnAnteriorUbicacion.UseVisualStyleBackColor = true;
            this.btnAnteriorUbicacion.Click += new System.EventHandler(this.btnAnteriorUbicacion_Click);
            // 
            // btnSiguienteUbicacion
            // 
            this.btnSiguienteUbicacion.Location = new System.Drawing.Point(351, 180);
            this.btnSiguienteUbicacion.Name = "btnSiguienteUbicacion";
            this.btnSiguienteUbicacion.Size = new System.Drawing.Size(75, 23);
            this.btnSiguienteUbicacion.TabIndex = 10;
            this.btnSiguienteUbicacion.Text = "Siguiente";
            this.btnSiguienteUbicacion.UseVisualStyleBackColor = true;
            this.btnSiguienteUbicacion.Click += new System.EventHandler(this.btnSiguienteUbicacion_Click);
            // 
            // lblUbicacion
            // 
            this.lblUbicacion.BackColor = System.Drawing.SystemColors.Control;
            this.lblUbicacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUbicacion.Location = new System.Drawing.Point(87, 20);
            this.lblUbicacion.Name = "lblUbicacion";
            this.lblUbicacion.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.lblUbicacion.Size = new System.Drawing.Size(339, 20);
            this.lblUbicacion.TabIndex = 6;
            this.lblUbicacion.Text = "C:\\";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(6, 72);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(420, 20);
            this.txtNombre.TabIndex = 5;
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(221, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Nombre del archivo de la copia de seguridad:";
            // 
            // btnUbicacion
            // 
            this.btnUbicacion.Location = new System.Drawing.Point(6, 20);
            this.btnUbicacion.Name = "btnUbicacion";
            this.btnUbicacion.Size = new System.Drawing.Size(75, 20);
            this.btnUbicacion.TabIndex = 3;
            this.btnUbicacion.Text = "Buscar";
            this.btnUbicacion.UseVisualStyleBackColor = true;
            this.btnUbicacion.Click += new System.EventHandler(this.btnUbicacion_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(242, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Seleccione la ubicación de la copia de seguridad:";
            // 
            // tabConfirmacion
            // 
            this.tabConfirmacion.Controls.Add(this.btnAnteriorConfirmacion);
            this.tabConfirmacion.Controls.Add(this.btnCopiar);
            this.tabConfirmacion.Controls.Add(this.txtResumen);
            this.tabConfirmacion.Controls.Add(this.label3);
            this.tabConfirmacion.Location = new System.Drawing.Point(4, 22);
            this.tabConfirmacion.Name = "tabConfirmacion";
            this.tabConfirmacion.Padding = new System.Windows.Forms.Padding(3);
            this.tabConfirmacion.Size = new System.Drawing.Size(432, 209);
            this.tabConfirmacion.TabIndex = 7;
            this.tabConfirmacion.Text = "Confirmación";
            this.tabConfirmacion.UseVisualStyleBackColor = true;
            // 
            // btnAnteriorConfirmacion
            // 
            this.btnAnteriorConfirmacion.Location = new System.Drawing.Point(204, 180);
            this.btnAnteriorConfirmacion.Name = "btnAnteriorConfirmacion";
            this.btnAnteriorConfirmacion.Size = new System.Drawing.Size(75, 23);
            this.btnAnteriorConfirmacion.TabIndex = 12;
            this.btnAnteriorConfirmacion.Text = "Anterior";
            this.btnAnteriorConfirmacion.UseVisualStyleBackColor = true;
            this.btnAnteriorConfirmacion.Click += new System.EventHandler(this.btnAnteriorConfirmacion_Click);
            // 
            // btnCopiar
            // 
            this.btnCopiar.Location = new System.Drawing.Point(285, 180);
            this.btnCopiar.Name = "btnCopiar";
            this.btnCopiar.Size = new System.Drawing.Size(141, 23);
            this.btnCopiar.TabIndex = 11;
            this.btnCopiar.Text = "Crear Copia de Seguridad";
            this.btnCopiar.UseVisualStyleBackColor = true;
            this.btnCopiar.Click += new System.EventHandler(this.btnCopiar_Click);
            // 
            // txtResumen
            // 
            this.txtResumen.Location = new System.Drawing.Point(9, 20);
            this.txtResumen.Name = "txtResumen";
            this.txtResumen.ReadOnly = true;
            this.txtResumen.Size = new System.Drawing.Size(417, 131);
            this.txtResumen.TabIndex = 3;
            this.txtResumen.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Resumen:";
            // 
            // tabGenerando
            // 
            this.tabGenerando.Controls.Add(this.btnFinalizar);
            this.tabGenerando.Controls.Add(this.pgbCopia);
            this.tabGenerando.Controls.Add(this.label4);
            this.tabGenerando.Location = new System.Drawing.Point(4, 22);
            this.tabGenerando.Name = "tabGenerando";
            this.tabGenerando.Padding = new System.Windows.Forms.Padding(3);
            this.tabGenerando.Size = new System.Drawing.Size(432, 209);
            this.tabGenerando.TabIndex = 8;
            this.tabGenerando.Text = "Generando";
            this.tabGenerando.UseVisualStyleBackColor = true;
            // 
            // pgbCopia
            // 
            this.pgbCopia.Location = new System.Drawing.Point(9, 86);
            this.pgbCopia.MarqueeAnimationSpeed = 0;
            this.pgbCopia.Name = "pgbCopia";
            this.pgbCopia.Size = new System.Drawing.Size(417, 23);
            this.pgbCopia.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pgbCopia.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(389, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Generando copia de seguridad, esto podría tomar algún tiempo. Por favor espere";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(439, 50);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.Enabled = false;
            this.btnFinalizar.Location = new System.Drawing.Point(351, 180);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(75, 23);
            this.btnFinalizar.TabIndex = 13;
            this.btnFinalizar.Text = "Finalizar";
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 285);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tabWizard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asistente de Copia de Seguridad";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabWizard.ResumeLayout(false);
            this.tabTipo.ResumeLayout(false);
            this.tabTipo.PerformLayout();
            this.tabTablas.ResumeLayout(false);
            this.tabTablas.PerformLayout();
            this.tabUbicacion.ResumeLayout(false);
            this.tabUbicacion.PerformLayout();
            this.tabConfirmacion.ResumeLayout(false);
            this.tabConfirmacion.PerformLayout();
            this.tabGenerando.ResumeLayout(false);
            this.tabGenerando.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabWizard;
        private System.Windows.Forms.TabPage tabTipo;
        private System.Windows.Forms.TabPage tabTablas;
        private System.Windows.Forms.RadioButton rdbTablas;
        private System.Windows.Forms.RadioButton rdbEsquema;
        private System.Windows.Forms.RadioButton rdbCompleta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabUbicacion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnUbicacion;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblUbicacion;
        private System.Windows.Forms.TabPage tabConfirmacion;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox cklTablas;
        private System.Windows.Forms.Button btnSiguienteTipo;
        private System.Windows.Forms.Button btnAnteriorTablas;
        private System.Windows.Forms.Button btnSiguienteTablas;
        private System.Windows.Forms.Button btnAnteriorUbicacion;
        private System.Windows.Forms.Button btnSiguienteUbicacion;
        private System.Windows.Forms.Button btnCopiar;
        private System.Windows.Forms.RichTextBox txtResumen;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAnteriorConfirmacion;
        private System.Windows.Forms.TabPage tabGenerando;
        private System.Windows.Forms.ProgressBar pgbCopia;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFinalizar;
    }
}

