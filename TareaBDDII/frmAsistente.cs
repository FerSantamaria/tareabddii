﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using Oracle.ManagedDataAccess.Client;
using System.Threading;

namespace TareaBDDII
{
    public partial class frmAsistente : Form
    {
        List<String> tablas = new List<string>();
        string strTablas = "";
        string ubicacion;
        string nombre;

        int[] completa = { 0, 1 };

        public frmAsistente()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            /* Ocultando tab headers */
            tabWizard.Appearance = TabAppearance.FlatButtons;
            tabWizard.ItemSize = new Size(0, 1);
            tabWizard.SizeMode = TabSizeMode.Fixed;

            OracleConnection conn = new OracleConnection("User Id=DEMs; Password=123456; Data Source=localhost:1521/orcl;");
            OracleCommand cmd = new OracleCommand("SELECT table_name FROM user_tables", conn);
            cmd.CommandType = CommandType.Text;
            conn.Open();

            DataSet ds = new DataSet();
            OracleDataAdapter oda = new OracleDataAdapter(cmd);
            oda.Fill(ds);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                tablas.Add(row.ItemArray[0].ToString());
            }

            conn.Close();

            foreach(String tabla in tablas)
            {
                cklTablas.Items.Add(tabla);
            }
        }



        private void btnUbicacion_Click(object sender, EventArgs e)
        {
            if ( folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                ubicacion = folderBrowserDialog1.SelectedPath;
                lblUbicacion.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            nombre = txtNombre.Text.Trim();

            Process expdp = new Process();
        }

        private void btnSiguienteTipo_Click(object sender, EventArgs e)
        {
            if (rdbCompleta.Checked || rdbEsquema.Checked)
            {
                tabWizard.SelectedTab = tabUbicacion;
            } else
            {
                tabWizard.SelectedTab = tabTablas;
            }
        }

        private void btnAnteriorTablas_Click(object sender, EventArgs e)
        {
            tabWizard.SelectedTab = tabTipo;
        }

        private void btnSiguienteTablas_Click(object sender, EventArgs e)
        {
            if(cklTablas.CheckedItems.Count > 0)
            {
                tablas.Clear();

                foreach (string tab in cklTablas.CheckedItems)
                {
                    tablas.Add(tab.ToString());
                }

                tabWizard.SelectedTab = tabUbicacion;
            }
            else
            {
                MessageBox.Show(this, "Debe Seleccionar al menos una tabla", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAnteriorUbicacion_Click(object sender, EventArgs e)
        {
            if (rdbCompleta.Checked || rdbEsquema.Checked)
            {
                tabWizard.SelectedTab = tabTipo;
            }
            else
            {
                tabWizard.SelectedTab = tabTablas;
            }
        }

        private void btnSiguienteUbicacion_Click(object sender, EventArgs e)
        {
            if(lblUbicacion.Text.Equals(""))
            {
                MessageBox.Show(this, "Debe Seleccionar una ubicación", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else if (txtNombre.Text.Trim().Equals(""))
            {
                MessageBox.Show(this, "Debe escribir un nombre", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else if (File.Exists(lblUbicacion.Text + "\\" + txtNombre.Text + ".dmp"))
            {
                MessageBox.Show(this, "Ya existe un archivo con ese nombre", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else
            {
                string str = "Tipo de Copia: ";

                if (rdbCompleta.Checked)
                {
                    str += "Copia completa\n";
                } else if (rdbEsquema.Checked)
                {
                    str += "Copia de esquema\n";
                } else
                {
                    str += "Copia de tablas\n";
                    str += "Tablas: ";

                    for (int i = 0; i < tablas.Count; i++)
                    {
                        if (i == (tablas.Count -1))
                        {
                            strTablas += tablas[i];
                        } else
                        {
                            strTablas += tablas[i] + ", ";
                        }
                    }

                    str += strTablas + "\n";
                }

                str += "Ubicación: " + lblUbicacion.Text + "\n";
                str += "Archivo: " + txtNombre.Text + ".dmp";

                txtResumen.Text = str;

                tabWizard.SelectedTab = tabConfirmacion;
                
            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == '.') || (e.KeyChar == ' '))
            {
                e.Handled = true;
            }
        }

        private void btnAnteriorConfirmacion_Click(object sender, EventArgs e)
        {
            tabWizard.SelectedTab = tabUbicacion;
        }

        private void btnCopiar_Click(object sender, EventArgs e)
        {
            tabWizard.SelectedTab = tabGenerando;

            pgbCopia.MarqueeAnimationSpeed = 20;

            nombre = txtNombre.Text.Trim();
            ubicacion = lblUbicacion.Text.Trim();

            string textoComando1 = "CREATE OR REPLACE DIRECTORY exportar as '" + lblUbicacion.Text + "'";
            string textoComando2 = "GRANT READ, WRITE ON DIRECTORY exportar to DEMs";

            using (OracleConnection conn = new OracleConnection("User Id=system; Password=123456; Data Source=localhost:1521/orcl;"))
            {
                conn.Open();
                using (OracleCommand comando1 = new OracleCommand(textoComando1, conn))
                {
                    comando1.ExecuteNonQuery();
                }

                using (OracleCommand comando2 = new OracleCommand(textoComando2, conn))
                {
                    comando2.ExecuteNonQuery();
                }
            }

            Process pr = new Process();
            pr.StartInfo.UseShellExecute = true;
            pr.StartInfo.CreateNoWindow = true;
            pr.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            pr.StartInfo.FileName = "expdp";
            
            if (rdbCompleta.Checked)
            {
                pr.StartInfo.Arguments = "system/123456 dumpfile="+ nombre + ".dmp logfile=" + nombre + ".log directory=exportar full=y compression=all";
            }
            else if (rdbEsquema.Checked)
            {
                pr.StartInfo.Arguments = "system/123456 schemas=DEMs dumpfile=" + nombre + ".dmp logfile=" + nombre + ".log directory=exportar";
            }
            else
            {
                pr.StartInfo.Arguments = "DEMs/123456 tables="+ strTablas +" dumpfile=" + nombre + ".dmp logfile=" + nombre + ".log directory=exportar";
            }

            pr.Start();

            pr.EnableRaisingEvents = true;
            pr.Exited += new EventHandler(finProceso);
        }

        void finProceso(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            fin();
            MessageBox.Show(this, "Respaldo de datos terminado", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnFinalizar_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        public void fin()
        {
            pgbCopia.MarqueeAnimationSpeed = 0;
            pgbCopia.Value = 0;
            btnFinalizar.Enabled = true;
        }
    }
}
