﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace TareaBDDII
{
    public partial class Form1 : Form
    {
        public string user_role;
        OracleConnection conn;

        public Form1()
        {
            InitializeComponent();
            conn = new OracleConnection();
            conn.ConnectionString = "User Id=DEMs; Password=123456; Data Source=localhost:1521/orcl;"; //Data Source Format -> //IP_HOST:PORT/SERVICE_NAME e.g. //127.0.0.1:1521/Service_Name
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
            this.AcceptButton = button1;
            
            conn.Open();
            label3.Text = string.Format("Conectado a ORACLE " + conn.ServerVersion);
            conn.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Text = "Conectando";
            button1.Enabled = false;
            OracleCommand cmd = new OracleCommand("SELECT * FROM usuarios WHERE username= : user_name and pass = :pswd", conn);
            cmd.Parameters.Add(new OracleParameter(":user_name", textBox1.Text));
            cmd.Parameters.Add(new OracleParameter(":pswd", textBox2.Text));
            conn.Open();

            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            da.Fill(ds);
            int i = ds.Tables[0].Rows.Count;
            if (i == 1)
            {
                user_role = ds.Tables[0].Rows[0]["PERM"].ToString();
                this.Hide();
                Form2 frm = new Form2(user_role);
                frm.Show();
                ds.Clear();
            }
            else
            {
                button1.Enabled = true;
                button1.Text = "Ingresar";
                MessageBox.Show(this, "Datos de inicio inválidos", "Error");
            }

            conn.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
