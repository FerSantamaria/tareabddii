﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace TareaBDDII
{
    public partial class Form2 : Form
    {
        public string role;
        OracleConnection conn;

        public Form2(string user_role)
        {
            InitializeComponent();
            role = user_role;
            conn = new OracleConnection();
            conn.ConnectionString = "User Id=DEMs; Password=123456; Data Source=localhost:1521/orcl;"; //Data Source Format -> //IP_HOST:PORT/SERVICE_NAME e.g. //127.0.0.1:1521/Service_Name

            if ( user_role.Equals("user"))
            {
                button2.Enabled = false;
                button3.Enabled = false;
                txtNombre.ReadOnly = true;
                txtApellido.ReadOnly = true;
            }
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form1 log = new Form1();
            log.Show();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.CenterToParent();
            label4.Text = string.Format("Conectado como: {0}", role);
            label5.Text = ""; label6.Text = "";
            if (role == "user") { button1.Enabled = false; label5.Text = "No Permitido"; textBox1.Enabled = false; textBox2.Enabled = false; }
            else label5.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {       
            if (textBox1.Text.Trim() == "" || textBox2.Text.Trim() == "")
            {
                MessageBox.Show(this, "No se permite insertar registros vacios", "Error");
            }
                else
            {
                OracleCommand cmd = new OracleCommand("insertar", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("nom", OracleDbType.Varchar2).Value = textBox1.Text;
                cmd.Parameters.Add("apell", OracleDbType.Varchar2).Value = textBox2.Text;

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                textBox1.Clear();
                textBox2.Clear();
                MessageBox.Show(this, "Registro Ingresado Correctamente.", "Información");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtId.Text.Trim() == "" || txtNombre.Text.Trim() == "" || txtApellido.Text.Trim() == "")
            {
                MessageBox.Show(this, "No se permite modificar registros vacios", "Error");
            }
            else
            {
                OracleCommand cmd = new OracleCommand("actualizar", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("cod", OracleDbType.Varchar2).Value = txtId.Text;
                cmd.Parameters.Add("nom", OracleDbType.Varchar2).Value = txtNombre.Text;
                cmd.Parameters.Add("apell", OracleDbType.Varchar2).Value = txtApellido.Text;

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                txtNombre.Clear();
                txtApellido.Clear();
                txtId.Clear();
                llenarGrid();
                MessageBox.Show(this, "Registro Actualizado", "Información");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txtId.Text.Trim() == "")
            {
                MessageBox.Show(this, "No se permite eliminar sin un Id", "Error");
            }
            else
            {
                OracleCommand cmd = new OracleCommand("eliminar", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("cod", OracleDbType.Varchar2).Value = txtId.Text;

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                txtId.Clear();
                llenarGrid();
                MessageBox.Show(this, "Usuario eliminado correctamente.", "Información");
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ( tabControl1.SelectedIndex == 1)
            {
                llenarGrid();
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                txtId.Text = row.Cells[0].Value.ToString();
                txtNombre.Text = row.Cells[1].Value.ToString();
                txtApellido.Text = row.Cells[2].Value.ToString();
            }
            
            if(dataGridView1.Rows.Count == 0)
            {
                txtId.Clear();
                txtNombre.Clear();
                txtApellido.Clear();
            }
        }

        private void llenarGrid()
        {
            try
            {
                OracleCommand cmd = new OracleCommand("SELECT * FROM registros", conn);
                conn.Open();
                DataSet ds = new DataSet();
                OracleDataAdapter da = new OracleDataAdapter(cmd);
                da.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];
                conn.Close();
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Ha ocurrido un error al mostrar los datos", "Error");
            }
        }
    }
}
